#include "opencv2/opencv.hpp"
#include <iostream>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <string.h>
#include <boost/thread.hpp>
#include <vector>
#include <QTime>
#include <cmath>
#include <cstdio>
#include <signal.h>

using namespace cv;
using namespace std;

bool checking_frames (int frame_number_1, int frame_number_2, int capture_time_1, int capture_time_2, int difference_time) // pobranie czasu, numerów klatek z kamer i zadanej różnicy czasu pomiędzy klatkami żeby zwróciło "true"
{
    if ((frame_number_1==frame_number_2) && (abs(capture_time_1-capture_time_2))< difference_time)
        return true;
    else
        return false;
}

void stream_camera (int camera_ID, int port)
{
        QTime time;
        time.start();
        int time_of_capture;

        int frame_number=0;

        int                 localSocket,
                            remoteSocket;
        struct  sockaddr_in localAddr,
                                    remoteAddr;

         int addrLen = sizeof(struct sockaddr_in);

         localSocket = socket(AF_INET , SOCK_STREAM , 0);
         if (localSocket == -1)
             cout<<"SOCKET CALL FAILED..."<<endl;

         localAddr.sin_family = AF_INET;
         localAddr.sin_addr.s_addr = INADDR_ANY;
         localAddr.sin_port = htons( port );

         if( bind(localSocket,(struct sockaddr *)&localAddr , sizeof(localAddr)) < 0)
         {
             cout <<"Socket can't be binded..."<<endl;
         }
         //Listening
         listen(localSocket , 1);

         //Waiting for connections
         cout <<  "Waiting for connections on port "<<  port << " for camera "<<camera_ID<<"..." <<endl;

         //accept connection from an incoming client
         remoteSocket = accept(localSocket, (struct sockaddr *)&remoteAddr, (socklen_t*)&addrLen);

         //acception failed
         if (remoteSocket < 0)
             cout<<"acceptation of connection failed!!"<<endl;
         else
             cout << "Connection accepted on camera " <<camera_ID<<" - port "<<port <<endl;

         //----------------------------------------------------------
         //OpenCV Code
         //----------------------------------------------------------

         VideoCapture cap(camera_ID); // open the camera on 'camera ID'
         Mat img, imgGray;
         img = Mat::zeros(480 , 640, CV_8UC3);

         //make it continuous
         if (!img.isContinuous()) {
             img = img.clone();
         }

         int imgSize = img.total() * img.elemSize();
         int bytes = 0;
         int key;


         //make img continuos
         if ( ! img.isContinuous() ) {
               img = img.clone();
               imgGray = img.clone();
         }

         cout << "Image Size:" << imgSize <<endl;

         while(1)
         {
                 /* get a frame from camera */
                     cap >> img;
                     frame_number++;

                     cout<<"frame number: "<<frame_number<<" - frame captured at: " <<time_of_capture<<" from camera "<<camera_ID<<endl;
                     //videoprocessing and opening window
    //                 imshow("Camera stream", img);
    //                 waitKey(10);
                     //send processed image
                     try{
                         if ((bytes = send(remoteSocket, img.data, imgSize, 0)) < 0){
                              cout<< "bytes = " << bytes <<endl;

                              break;
                         }
                     }
                     catch(...){
                         cout<< "Broken pipe"<<endl;
                         break;
                     }
         }
         cout<<endl<<"Closing socket..."<<endl;
        close(remoteSocket);
        cap.release();

}

void handler(int s) {
    printf("Caught SIGPIPE\n");
}


int main(int argc, char** argv)
{
//        signal(SIGPIPE, handler);
        boost::thread cam1 (stream_camera, 0, 8001);
        boost::thread cam2 (stream_camera, 1, 8002);
        while (getchar()!='q');

        sleep (2);

        return 0;
}
