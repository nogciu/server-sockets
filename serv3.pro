#-------------------------------------------------
#
# Project created by QtCreator 2015-09-02T11:18:12
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = serv3
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

LIBS += \
       -lboost_system\
        -lboost_thread\
-L/usr/local/lib \
-lopencv_core \
-lopencv_imgproc \
-lopencv_highgui \
-lopencv_ml \
-lopencv_video \
-lopencv_features2d \
-lopencv_calib3d \
-lopencv_objdetect \
-lopencv_contrib \
-lopencv_legacy \
-lopencv_flann
